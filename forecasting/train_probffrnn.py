""" Train a feedforward neural net whose output units parameterize a multi-dimensional mixture of Gaussians. """
import sys
import joblib
import numpy as np
import theano 
from theano import tensor as T
import lasagne
from lasagne.layers import batch_norm, dropout
from lasagne.regularization import regularize_layer_params_weighted
from lasagne.regularization import l1 as l1_regularization
from helpers import gen_batch_simple, logsumexp

LOAD_SAVED=False
NUM_EPOCHS=10000
BATCHES_PER_EPOCH=32
BATCH_SIZE=64
SEQ_LEN=60
PRED_LEN=10
RANK=2
grad_clip = 1.
N_COMPONENTS=10 # Number of componbents in the density mixture
nonlin = lasagne.nonlinearities.rectify
load_fname = 'net_params/pnn'
save_fname = 'net_params/pnn3'

if LOAD_SAVED:
    net = joblib.load(load_fname)
else:
    # Assemble the network.
    l0 = lasagne.layers.InputLayer(shape=(None, 1, SEQ_LEN))
    l1 = lasagne.layers.DenseLayer(l0, num_units=64, nonlinearity=nonlin)
    l2 = lasagne.layers.DenseLayer(l1, num_units=128, nonlinearity=nonlin)
    l3 = lasagne.layers.DenseLayer(l2, num_units=256, nonlinearity=nonlin)
    l4 = lasagne.layers.DenseLayer(l2, num_units=512, nonlinearity=nonlin)
    net = lasagne.layers.DenseLayer(l4, num_units=N_COMPONENTS+N_COMPONENTS*PRED_LEN+N_COMPONENTS*PRED_LEN*RANK, 
        nonlinearity=lasagne.nonlinearities.identity)

    # l0 = lasagne.layers.InputLayer(shape=(None, 1, SEQ_LEN))
    # l1c = lasagne.layers.Conv1DLayer(l0, num_filters=128, filter_size=6, nonlinearity=nonlin)
    # l1p = lasagne.layers.MaxPool1DLayer(l1c, pool_size=4, stride=2)
    # l2c = lasagne.layers.Conv1DLayer(l1p, num_filters=256, filter_size=6, nonlinearity=nonlin)
    # l2p = lasagne.layers.MaxPool1DLayer(l2c, pool_size=4, stride=2)
    # l3 = lasagne.layers.DenseLayer(l2p, num_units=128, nonlinearity=nonlin)
    # l4 = lasagne.layers.DenseLayer(l3, num_units=64, nonlinearity=nonlin) 
    # net = lasagne.layers.DenseLayer(l4, num_units=3*N_COMPONENTS*(1+int(WATER)), 
    #     nonlinearity=lasagne.nonlinearities.identity)


# Assemble the training algorithm.
print('Compiling the theano net...')
input_var = lasagne.layers.get_all_layers(net)[0].input_var
target_var = T.matrix('target_output')

# The cost function, log likelihood of the data under the mixture model.
oil_out = lasagne.layers.get_output(net, deterministic=False)

oil_alphas = T.nnet.softmax(oil_out[:,:N_COMPONENTS])
oil_means = oil_out[:,N_COMPONENTS:(N_COMPONENTS+N_COMPONENTS*PRED_LEN)]
oil_sigmas = T.nnet.softplus(oil_out[:,(N_COMPONENTS+N_COMPONENTS*PRED_LEN):])
oil_sigmas = oil_sigmas.reshape((N_COMPONENTS, PRED_LEN, RANK))
oil_sigmas = np.array([T.dot(sigmas, sigmas.T) for sigmas in oil_sigmas])
oil_Eq = T.log(oil_alphas/(T.sqrt(2*np.pi)*oil_sigmas))-\
        (T.tile(target_var[:,0:1], (1, N_COMPONENTS))-oil_means)**2/(2*oil_sigmas**2)
oil_cost = -logsumexp(oil_Eq, axis=1).sum()
l_weights = {l1: 1e-5, l2: 1e-4, l3: 1e-3, l4: 1e-4}
l1_penalty = regularize_layer_params_weighted(l_weights, l1_regularization)
tr_cost = oil_cost + l1_penalty
val_cost = oil_cost

params = lasagne.layers.get_all_params(net, trainable=True)
updates = lasagne.updates.adam(tr_cost, params, learning_rate=.0001)
train = theano.function([input_var, target_var], 
    tr_cost, updates=updates, allow_input_downcast=True)
validate = theano.function([input_var, target_var], 
    val_cost, allow_input_downcast=True)

# Train the network.
print('Done. Training...')
X_tst, y_tst = gen_batch_simple(batch_size=BATCH_SIZE*BATCHES_PER_EPOCH, 
    seq_len=SEQ_LEN, min_seq_len=SEQ_LEN, which='test', log=True)

best_loss = np.inf
for epoch_id in range(NUM_EPOCHS):
    try:
        tr_loss = 0
        for batch_id in range(BATCHES_PER_EPOCH):
            X_tr, y_tr = gen_batch_simple(batch_size=BATCH_SIZE, seq_len=SEQ_LEN, 
                min_seq_len=SEQ_LEN, which='train', log=True)
            tr_loss += train(X_tr, y_tr)
        val_loss = validate(X_tst, y_tst)
        if np.isfinite(val_loss) and val_loss < best_loss:
            joblib.dump(net, save_fname)
            best_loss = val_loss
        print('Epoch {}. Valid NLL {}. Train NLL {}'.format(epoch_id, 
            np.sqrt(val_loss/float(BATCHES_PER_EPOCH)), np.sqrt(tr_loss/BATCHES_PER_EPOCH)))
    except KeyboardInterrupt:
        print('Training interrupted.')
        break
