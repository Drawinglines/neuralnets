""" Train a recurrent neural network whose outputs parameterize a 1d mixture of Gaussians. """
import sys
import joblib
import numpy as np
import theano 
from theano import tensor as T
import lasagne

from helpers import gen_batch, logsumexp

LOAD_SAVED=False
WATER=False
NUM_EPOCHS=10000
BATCHES_PER_EPOCH=32
BATCH_SIZE=64
SEQ_LEN=60
grad_clip = 1.
N_COMPONENTS=4 # Number of componbents in the density mixture
nonlin = lasagne.nonlinearities.tanh
load_fname = 'net_params/prnn_gradclip10'
save_fname = 'net_params/prnn_gradclip10'

if LOAD_SAVED:
    net = joblib.load(load_fname)
else:
    # Assemble the network.
    l0 = lasagne.layers.InputLayer(shape=(None, SEQ_LEN, 1+int(WATER)))
    gate_parameters = lasagne.layers.recurrent.Gate(W_in=lasagne.init.Orthogonal(), 
        W_hid=lasagne.init.Orthogonal(), b=lasagne.init.Constant(0.))
    cell_parameters = lasagne.layers.recurrent.Gate(W_in=lasagne.init.Orthogonal(), 
        W_hid=lasagne.init.Orthogonal(), W_cell=None, b=lasagne.init.Constant(0.), 
            nonlinearity=lasagne.nonlinearities.tanh)
    lmask = lasagne.layers.InputLayer(shape=(None, SEQ_LEN))
    l1 = lasagne.layers.LSTMLayer(l0, 32, grad_clipping=grad_clip, 
                                  nonlinearity=nonlin, learn_init=True, mask_input=lmask,
                                  ingate=gate_parameters, forgetgate=gate_parameters,
                                  cell=cell_parameters, outgate=gate_parameters)
    l2 = lasagne.layers.LSTMLayer(l1, 32, grad_clipping=grad_clip, 
                                  nonlinearity=nonlin, learn_init=True,
                                  ingate=gate_parameters, forgetgate=gate_parameters,
                                  cell=cell_parameters, outgate=gate_parameters)
    l3 = lasagne.layers.LSTMLayer(l2, 32, grad_clipping=grad_clip,
                                  nonlinearity=nonlin, learn_init=True)

    l4 = lasagne.layers.SliceLayer(l3, indices=-1, axis=1)
    net = lasagne.layers.DenseLayer(l4, num_units=3*N_COMPONENTS*(1+int(WATER)), 
        nonlinearity=lasagne.nonlinearities.identity)

# Assemble the training algorithm.
print('Compiling the theano net...')
input_var = lasagne.layers.get_all_layers(net)[0].input_var
mask_var = lasagne.layers.get_all_layers(net)[1].input_var
target_var = T.matrix('target_output')

# The cost function, log likelihood of the data under the mixture model.
if WATER:
    oil_out = lasagne.layers.get_output(net)[:,:N_COMPONENTS*3]
else:
    oil_out = lasagne.layers.get_output(net)

oil_alphas = T.nnet.softmax(oil_out[:,:N_COMPONENTS])
oil_means = oil_out[:,N_COMPONENTS:N_COMPONENTS*2]
oil_sigmas = 1e-5+T.nnet.softplus(oil_out[:,N_COMPONENTS*2:N_COMPONENTS*3])
oil_Eq = T.log(oil_alphas/(T.sqrt(2*np.pi)*oil_sigmas))-\
        (T.tile(target_var[:,0:1], (1, N_COMPONENTS))-oil_means)**2/(2*oil_sigmas**2)
oil_cost = -logsumexp(oil_Eq, axis=1).sum()

if WATER:
    water_out = lasagne.layers.get_output(net)[:,N_COMPONENTS*3:]
    water_alphas = T.nnet.softmax(water_out[:,:N_COMPONENTS])
    water_means = water_out[:,N_COMPONENTS:N_COMPONENTS*2]
    water_sigmas = 1e-5+T.nnet.softplus(water_out[:,N_COMPONENTS*2:N_COMPONENTS*3])
    water_Eq = T.log(water_alphas/(T.sqrt(2*np.pi)*water_sigmas))-\
            (T.tile(target_var[:,1:2], (1, N_COMPONENTS))-water_means)**2/(2*water_sigmas**2)
    water_cost = -logsumexp(water_Eq, axis=1).sum()

tr_cost = oil_cost+water_cost if WATER else oil_cost

params = lasagne.layers.get_all_params(net, trainable=True)
updates = lasagne.updates.adam(tr_cost, params, learning_rate=.0001)
train = theano.function([input_var, target_var, mask_var], 
    tr_cost, updates=updates, allow_input_downcast=True)
validate = theano.function([input_var, target_var, mask_var], 
    tr_cost, allow_input_downcast=True)

# Train the network.
print('Done. Training...')
X_tst, y_tst, mask_tst = gen_batch(batch_size=BATCH_SIZE*BATCHES_PER_EPOCH, 
    seq_len=SEQ_LEN, min_seq_len=SEQ_LEN, which='test', water=WATER, log=True)
best_loss = np.inf
for epoch_id in range(NUM_EPOCHS):
    try:
        tr_loss = 0
        for batch_id in range(BATCHES_PER_EPOCH):
            X_tr, y_tr, mask_tr = gen_batch(batch_size=BATCH_SIZE, seq_len=SEQ_LEN, 
                min_seq_len=SEQ_LEN, which='train', water=WATER, log=True)
            tr_loss += train(X_tr, y_tr, mask_tr)
        val_loss = validate(X_tst, y_tst, mask_tst)
        if np.isfinite(val_loss) and val_loss < best_loss:
            joblib.dump(net, save_fname)
            best_loss = val_loss
        print('Epoch {}. Valid NLL {}. Train NLL {}'.format(epoch_id, 
            np.sqrt(val_loss/float(BATCHES_PER_EPOCH)), np.sqrt(tr_loss/BATCHES_PER_EPOCH)))
    except KeyboardInterrupt:
        print('Training interrupted.')
        break
