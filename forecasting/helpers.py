import sys
import calendar
import theano
import numpy as np
from scipy.optimize import minimize
import MySQLdb
from theano import tensor as T
import lasagne
from lasagne.regularization import regularize_layer_params_weighted
from lasagne.regularization import regularize_network_params
from lasagne.regularization import l1 as l1_regularization
from lasagne.regularization import l2 as l2_regularization
import rezas

allowed_ids = np.array(open('allowed_ids.txt', 'r').read().split())
cnx = {'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com', 'user': 'kris',
       'passwd': 'datascience', 'db': 'public_data', 'port': 3306}
conn = MySQLdb.connect(**cnx)
cursor = conn.cursor()

def convert_date(abs_date, year0=2009):
    year = int(abs_date[:4])
    month = int(abs_date[5:7])
    return (year-year0)*12+month-1

def revert_date(time_from_year0, year0=2008):
    month = np.mod(time_from_year0, 12)+1
    year = int(time_from_year0)/12
    return '{}-{:02d}-{:02d}'.format(year+year0, month, calendar.monthrange(year+year0, month)[1])

def gen_batch_simple(batch_size=32, seq_len=12, min_seq_len=12, 
                     ignore_cfs=True, which='train', log=True):
    """ Yield a batch of well data: sequences of water and oil 
    production of given length, and production from the month 
    following the last month in given sequence as the target. """
    x = np.zeros((batch_size, seq_len, 1))
    y = np.zeros((batch_size, 1))
    for x_id in range(batch_size):
        xym = get_oil_simple(seq_len=seq_len, min_seq_len=min_seq_len, 
                             ignore_cfs=True, which=which)
        (x[x_id], y[x_id]) = xym
    x = x.transpose([0,2,1])
    return (x, y)

def gen_batch(batch_size=32, seq_len=12, min_seq_len=12, ignore_cfs=True, which='train', water=False, log=True, seq_dim=1):
    """ Yield a batch of well data: sequences of water and oil production of given length,
    and production from the month following the last month in given sequence as the target. """
    x = np.zeros((batch_size, seq_len, 1+int(water)))
    y = np.zeros((batch_size, 1+int(water)))
    m = np.zeros((batch_size, seq_len))
    for x_id in range(batch_size):
        xym = get_oil_data(seq_len=seq_len, min_seq_len=min_seq_len, 
                           ignore_cfs=True, which=which, get_water=water)
        (x[x_id], y[x_id], m[x_id]) = xym
    if seq_dim==2:
        x = x.transpose([0,2,1])
    return (x, y, m)

def get_oil_data(seq_len=12, min_seq_len=12, ignore_cfs=True, 
                 which='train', get_water=True, log=True, well_id=None):
    """ Query the db for a valid well, return a subsequence of its oil and water
    production, and a prediction target. """
    # Sample wells with replacement (each time we'll use a different subseqence).
    while True:
        # Data goes here.
        oil_water_masked = np.zeros((seq_len, 1+int(get_water)))

        # Choose a well at random.
        if well_id is None:
            well_id = np.random.choice(allowed_ids, 1)[0]

        # Fetch well data from the db.
        query = 'SELECT oil, water, dates, proddays FROM bigprod_ts WHERE API_prod = '+well_id
        cursor.execute(query)
        data = cursor.fetchall()
        if data[0][0] is not None and data[0][1] is not None and data[0][2] is not None:
            oil = np.asarray(map(float, data[0][0].split(',')))
            water = np.asarray(map(float, data[0][1].split(','))) 
            dates = np.asarray(map(convert_date, data[0][2].split(',')))
        else:
            continue
            
        # Convert NULLs and INFs to zeros.
        oil[np.isnan(oil)] = 0.
        oil[np.isinf(oil)] = 0.
        water[np.isnan(water)] = 0.
        water[np.isinf(water)] = 0.

        # Ignore any trailing zeros (presumably catastrophic failures).
        if ignore_cfs:
            trailing_zeros = np.where(np.cumsum(np.fliplr(np.atleast_2d(oil)))==0)[0]
            if trailing_zeros.size > 0:
                oil = oil[:-trailing_zeros[-1]-1]
                water = water[:-trailing_zeros[-1]-1]
                dates = dates[:-trailing_zeros[-1]-1]

        if which=='test':
            # If testing, only keep points adjecent to the training period.
            first_test = np.where(dates >= 0)[0]
            if first_test.size == 0 or first_test[0] < min_seq_len:
                oil = np.array([])
                water = np.array([])
            else:
                oil = oil[first_test[0]-seq_len:]
                water = water[first_test[0]-seq_len:]

        elif which=='train':
            # Ignore data from over 20 years ago.
            first_train = np.where(dates >= -12*20)[0]
            # Remove any points past the training period.
            last_train = np.where(dates < 0)[0]
            if last_train.size==0 or first_train.size==0 or last_train[-1]-first_train[0]<seq_len:
                oil = np.array([])
                water = np.array([])
            else:
                oil = oil[first_train[0]:last_train[-1]+1]
                water = water[first_train[0]:last_train[-1]+1]

        # Choose a subsequence and format it into shapely arrays.
        if oil.size > min_seq_len+1:
            if oil.size > seq_len+1:
                start_id = np.random.choice(oil.size-seq_len-1)
                oil_water_masked[:,0] = oil[start_id:start_id+seq_len]
                if get_water:
                    oil_water_masked[:,1] =water[start_id:start_id+seq_len]
                    oil_water_target = np.hstack([oil[start_id+seq_len+1:start_id+seq_len+2],
                                              water[start_id+seq_len+1:start_id+seq_len+2]])
                else:
                    oil_water_target = oil[start_id+seq_len+1:start_id+seq_len+2]
                mask = np.ones(seq_len)
            else:
                oil_water_masked[:oil.size-1,0] = oil[:-1]
                if get_water:
                    oil_water_masked[:oil.size-1,1] = water[:-1]
                    oil_water_target = np.hstack([oil[-1:], water[-1:]])
                else:
                    oil_water_target = oil[-1:]
                mask = np.hstack([np.ones(oil.size-1), np.zeros(seq_len-oil.size+1)])
            if log:
                return (np.log(1+oil_water_masked), np.log(1+oil_water_target), mask)
            else:
                return (oil_water_masked, oil_water_target, mask)

def fetch_well(well_id, seq_len=60, min_seq_len=60, ignore_cfs=True, 
                   which='train', log=True, pred_len=7*12):
        # Data goes here.
        oil_data = np.zeros((seq_len, 1))

        # Fetch well data from the db.
        query = 'SELECT oil, dates, proddays FROM bigprod_ts WHERE API_prod = '+well_id
        cursor.execute(query)
        data = cursor.fetchall()
        if data[0][0] is not None and data[0][1] is not None:
            oil = np.asarray(map(float, data[0][0].split(',')))
            dates = np.asarray(map(convert_date, data[0][1].split(',')))
            proddays = np.asarray(map(float, data[0][2].split(',')))
        else:
            return None, None, None

        # Convert NULLs and INFs to zeros.
        oil[np.isnan(oil)] = 0.
        oil[np.isinf(oil)] = 0.
        oil[oil<0] = 0.

        if which=='test':
            # If testing, only keep points adjecent to the training period.
            first_test = np.where(dates >= 0)[0]
            if first_test.size == 0 or first_test[0] < min_seq_len:
                return None, None, None
            else:
                oil = oil[first_test[0]-seq_len:]

        elif which=='train':
            # Remove any points past the training period.
            last_train = np.where(dates < 0)[0]
            if last_train.size==0 or last_train[-1]-dates[0]<seq_len:
                return None, None, None
            else:
                oil = oil[:last_train[-1]+1]

        # Choose a subsequence and format it into shapely arrays.
        if oil.size > min_seq_len+1:
            if oil.size > seq_len+1:
                start_id = np.random.choice(oil.size-seq_len-1)
                oil_data[:,0] = oil[start_id:start_id+seq_len]
                oil_target = oil[start_id+seq_len+1:start_id+seq_len+2]
            else:
                oil_data[:oil.size-1,0] = oil[:-1]
                oil_target = oil[-1:]

            # Ignore trailing zeros (presumably catastrophic failures).
            if ignore_cfs:
                if oil_data[-1,0] == 0:
                    return None, None, None

            if log:
                return np.log(1+oil_data), np.log(1+oil_target), proddays
            else:
                return oil_data, oil_target, proddays
        else:
            return None, None, None

def get_oil_simple(seq_len=12, min_seq_len=12, ignore_cfs=True, 
                   which='train', log=True, pred_len=1, well_id=None):
    """ Query the db for a valid well, return a subsequence of its oil and water
    production, and a prediction target. """
    # Sample wells with replacement (each time we'll use a different subseqence).
    while True:
        # Data goes here.
        oil_data = np.zeros((seq_len, 1))

        # Choose a well at random.
        well_id = np.random.choice(allowed_ids, 1)[0]

        # Fetch well data from the db.
        query = 'SELECT oil, dates, proddays FROM bigprod_ts WHERE API_prod = '+well_id
        cursor.execute(query)
        data = cursor.fetchall()
        if data[0][0] is not None and data[0][1] is not None:
            oil = np.asarray(map(float, data[0][0].split(',')))
            dates = np.asarray(map(convert_date, data[0][1].split(',')))
        else:
            continue

        # Convert NULLs and INFs to zeros.
        oil[np.isnan(oil)] = 0.
        oil[np.isinf(oil)] = 0.
        oil[oil<0] = 0.

        if which=='test':
            # If testing, only keep points adjecent to the training period.
            first_test = np.where(dates >= 0)[0]
            if first_test.size == 0 or first_test[0] < min_seq_len:
                continue
            else:
                oil = oil[first_test[0]-seq_len:]

        elif which=='train':
            # Remove any points past the training period.
            last_train = np.where(dates < 0)[0]
            if last_train.size==0 or last_train[-1]-dates[0]<seq_len:
                continue
            else:
                oil = oil[:last_train[-1]+1]

        # Choose a subsequence and format it into shapely arrays.
        if oil.size > min_seq_len+1:
            if oil.size > seq_len+1:
                start_id = np.random.choice(oil.size-seq_len-1)
                oil_data[:,0] = oil[start_id:start_id+seq_len]
                oil_target = oil[start_id+seq_len+1:start_id+seq_len+2]
            else:
                oil_data[:oil.size-1,0] = oil[:-1]
                oil_target = oil[-1:]

            # Ignore trailing zeros (presumably catastrophic failures).
            if ignore_cfs:
                if oil_data[-1,0] == 0:
                    continue

            if log:
                return (np.log(1+oil_data), np.log(1+oil_target))
            else:
                return (oil_data, oil_target)

def compile_probnn(net, n_components, reg_coeff=1e-5):
    """ Compile a probabilistic neural net in Lasagne. """
    net = lasagne.layers.DenseLayer(net, num_units=3*n_components, 
        nonlinearity=lasagne.nonlinearities.identity)
    input_var = lasagne.layers.get_all_layers(net)[0].input_var
    target_var = T.matrix('target_output')

    # The cost function, log likelihood of the data under the mixture model.
    oil_out = lasagne.layers.get_output(net, deterministic=False)
    oil_alphas = T.nnet.softmax(oil_out[:,:n_components])
    oil_means = oil_out[:,n_components:n_components*2]
    oil_sigmas = 1e-7+T.nnet.softplus(oil_out[:,n_components*2:n_components*3])
    oil_Eq = T.log(oil_alphas/(T.sqrt(2*np.pi)*oil_sigmas))-\
            (T.tile(target_var[:,0:1], (1, n_components))-oil_means)**2/(2*oil_sigmas**2)
    oil_cost = -logsumexp(oil_Eq, axis=1).sum()
    l2_penalty = regularize_network_params(net, l2_regularization)*reg_coeff
    tr_cost = oil_cost + l2_penalty

    oil_out_val = lasagne.layers.get_output(net, deterministic=True)
    oil_alphas_val = T.nnet.softmax(oil_out_val[:,:n_components])
    oil_means_val = oil_out_val[:,n_components:n_components*2]
    oil_sigmas_val = 1e-7+T.nnet.softplus(oil_out_val[:,n_components*2:n_components*3])
    oil_Eq_val = T.log(oil_alphas_val/(T.sqrt(2*np.pi)*oil_sigmas_val))-\
            (T.tile(target_var[:,0:1], (1, n_components))-oil_means_val)**2/(2*oil_sigmas_val**2)
    oil_cost_val = -logsumexp(oil_Eq_val, axis=1).sum()
    val_cost = oil_cost_val

    learning_rate = T.scalar(name='learning_rate')
    params = lasagne.layers.get_all_params(net, trainable=True)
    updates = lasagne.updates.adam(tr_cost, params, learning_rate)
    train = theano.function([input_var, target_var, learning_rate], 
        tr_cost, updates=updates, allow_input_downcast=True)
    validate = theano.function([input_var, target_var], 
        val_cost, allow_input_downcast=True)

    return net, train, validate

def compile_probnn_fullout(net, n_components, reg_coeff=1e-5, outlen=7*12):
    """ Compile a probabilistic neural net in Lasagne. The output of the net
    is a vector of 7 year-long prediction. """
    net = lasagne.layers.DenseLayer(net, num_units=n_components*(2+outlen), 
        nonlinearity=lasagne.nonlinearities.identity)
    input_var = lasagne.layers.get_all_layers(net)[0].input_var
    target_var = T.matrix('target_output')

    # The cost function, log likelihood of the data under the mixture model.
    oil_out = lasagne.layers.get_output(net, deterministic=False)
    oil_alphas = T.nnet.softmax(oil_out[:,:n_components])
    oil_sigmas = 1e-7+T.nnet.softplus(oil_out[:,n_components:n_components*2])
    oil_means = oil_out[:,n_components*2:]

    oil_Eq = T.log(oil_alphas/(T.sqrt(2*np.pi)*oil_sigmas)**outlen)-\
        (T.tile(target_var[:,0:1], (1, n_components))-oil_means).norm(L=2, axis=0)**2/(2*oil_sigmas**2)
    oil_cost = -logsumexp(oil_Eq, axis=1).sum()
    l2_penalty = regularize_network_params(net, l2_regularization)*reg_coeff
    tr_cost = oil_cost + l2_penalty

    # The validation loss (same as above but takes batch norm and dropout into account.)
    oil_out_val = lasagne.layers.get_output(net, deterministic=True)
    oil_alphas_val = T.nnet.softmax(oil_out_val[:,:n_components])
    oil_sigmas_val = 1e-7+T.nnet.softplus(oil_out_val[:,n_components:n_components*2])
    oil_means_val = oil_out_val[:,n_components*2:]

    oil_Eq_val = T.log(oil_alphas_val/(T.sqrt(2*np.pi)*oil_sigmas_val)**outlen)-\
        (T.tile(target_var[:,0:1], (1, n_components))-
         oil_means_val).norm(L=2, axis=0)**2/(2*oil_sigmas_val**2)
    oil_cost_val = -logsumexp(oil_Eq_val, axis=1).sum()
    val_cost = oil_cost_val

    learning_rate = T.scalar(name='learning_rate')
    params = lasagne.layers.get_all_params(net, trainable=True)
    updates = lasagne.updates.adam(tr_cost, params, learning_rate)
    train = theano.function([input_var, target_var, learning_rate], 
        tr_cost, updates=updates, allow_input_downcast=True)
    validate = theano.function([input_var, target_var], 
        val_cost, allow_input_downcast=True)

    return train, validate


def logsumexp(x, axis=None, keepdims=True):
    xmax = T.max(x, axis=axis, keepdims=True)
    preres = T.log(T.sum(T.exp(x-xmax), axis=axis, keepdims=keepdims))
    return preres+xmax.reshape(preres.shape)

def np_logsumexp(x, axis=None, keepdims=True):
    xmax = np.max(x, axis=axis, keepdims=True)
    preres = np.log(np.sum(np.exp(x-xmax), axis=axis, keepdims=keepdims))
    return preres+xmax.reshape(preres.shape)

def softplus(x, bias=0):
    return np.log(1+np.exp(x-bias))

def softmax(x, bias=0):
    return np.exp(x*(1+bias))/np.exp(x*(1+bias)).sum()

def sample_gaussian_mixture(params, bias=0, n_samples=1, expected=False):
    n_components = params.size/3
    alphas = softmax(params.flatten()[:n_components], bias)
    means = params.flatten()[n_components:n_components*2]
    sigmas = 1e-5+softplus(params.flatten()[n_components*2:n_components*3], bias)
    sigmas[np.where(sigmas>10)[0]]=10.

    # Choose the mixture
    alphas = alphas/alphas.sum()
    mix_ids = np.random.choice(np.arange(n_components), 
        size=n_samples, p=alphas, replace=True)
    samples = np.atleast_2d(sigmas[mix_ids]*np.random.randn(n_samples)+means[mix_ids]).T
    liks = np_logsumexp(np.array([-(samples-means[mid])**2/(2*sigmas[mid]**2)
                                  for mid in range(n_components)]), axis=0).flatten()
    if expected:
        return samples.mean()
    else:
        return samples[np.argmax(liks)][0]

def eval_gaussian_mixture(params, bias, density_grid=np.linspace(0, 1000, 1000)):
    density_grid = np.atleast_2d(density_grid).T
    n_components = params.size/3
    alphas = softmax(params.flatten()[:n_components], bias)
    means = params.flatten()[n_components:n_components*2]
    sigmas = 1e-5+softplus(params.flatten()[n_components*2:n_components*3], bias)
    sigmas[np.where(sigmas>10)[0]]=10.

    component_wise = alphas * np.exp(-.5 * (density_grid - means)**2 / sigmas**2) / (sigmas * np.sqrt(2 * np.pi))
    return component_wise.sum(axis=1)

def predict_prnn(predictor, x, mask, num_steps, n_samples=10000, bias=3):
    params = predictor(x, mask)
    xbest = [sample_gaussian_mixture(params, bias=bias, n_samples=n_samples)]
    x = np.hstack([x, [[xbest]]])
    mask = np.hstack([mask, [[1]]])
    for step_id in range(num_steps-1):
        sys.stdout.write('\rStep {}/{}.'.format(step_id, num_steps))
        sys.stdout.flush()
        params = predictor(x, mask)
        xbest = [sample_gaussian_mixture(params, bias=bias, n_samples=n_samples)]
        x = np.hstack([x, [[xbest]]])
        mask = np.hstack([mask, [[1]]])
    return x

def predict_pnn(predictor, x, num_steps, n_samples=10000, bias=0, density_grid=np.linspace(0,1000,1000), expected=False):
    params = predictor(x)
    seq_len = x.shape[2]
    xbest = sample_gaussian_mixture(params, bias=bias, n_samples=n_samples, expected=expected)
    x = np.concatenate([x, [[[xbest]]]], 2)
    density = [eval_gaussian_mixture(params, bias=bias, density_grid=density_grid)]
    for step_id in range(num_steps-1):
        params = predictor(x[:,:,-seq_len:])
        xbest = sample_gaussian_mixture(params, bias=bias, n_samples=n_samples, expected=expected)
        x = np.concatenate([x, [[[xbest]]]], 2)
        density.append(eval_gaussian_mixture(params, bias=bias, density_grid=density_grid))
    return x, density

    
if __name__=='__main__':
    print('Downloading a batch of size 1, seq_len 1...')
    print(gen_batch(1, 120, 120, which='test')[:2])
