""" Train a feedforward neural net whose output
units parameterize a 1d mixture of Gaussians. """
import sys
import joblib
import numpy as np
import lasagne
from lasagne.layers import batch_norm
from helpers import gen_batch_simple
from helpers import compile_probnn

sys.setrecursionlimit(10000)
LOAD_SAVED = False
NUM_EPOCHS = 100000
BATCHES_PER_EPOCH = 32
BATCHNORM = True
BATCH_SIZE = 128
SEQ_LEN = 60
N_COMPONENTS = 2 # Number of components in the density mixture
LEARNING_RATE = 3*1e-5 # Starting learning rate.
LOAD_FNAME = 'net_params/2gauss_net_large'
SAVE_FNAME = 'net_params/2gauss_net_large'

def train_probnn():
    """ Train a probabilistic neural network.

    Args:
        None

    Returns:
        None
    """
    if LOAD_SAVED:
        (net, _) = joblib.load(LOAD_FNAME)
    else:
        rct = lasagne.nonlinearities.rectify
        # Assemble the network.
        nin = lasagne.layers.InputLayer(shape=(None, 1, SEQ_LEN))
        if BATCHNORM:
            nin = batch_norm(nin)
        net = lasagne.layers.DenseLayer(nin, num_units=1024, nonlinearity=rct)
        if BATCHNORM:
            net = batch_norm(net)
        net = lasagne.layers.DenseLayer(net, num_units=1024, nonlinearity=rct)
        if BATCHNORM:
            net = batch_norm(net)
        net = lasagne.layers.DenseLayer(net, num_units=1024, nonlinearity=rct)
        if BATCHNORM:
            net = batch_norm(net)
        net = lasagne.layers.DenseLayer(net, num_units=1024, nonlinearity=rct)
        if BATCHNORM:
            net = batch_norm(net)
        net = lasagne.layers.DenseLayer(net, num_units=1024, nonlinearity=rct)
        if BATCHNORM:
            net = batch_norm(net)
        net = lasagne.layers.DenseLayer(net, num_units=1024, nonlinearity=rct)
        if BATCHNORM:
           net = batch_norm(net)


    # Assemble the training algorithm.
    print 'Compiling the theano net...'
    net, train, validate = compile_probnn(net, N_COMPONENTS)

    # Train the network.
    print 'Done. Training...'
    np.random.seed(1423)
    x_tst, y_tst = gen_batch_simple(batch_size=10000, seq_len=SEQ_LEN,
                                    min_seq_len=SEQ_LEN, which='test', log=True)
    best_loss = np.inf
    loss_history = np.zeros((2, NUM_EPOCHS))
    for epoch_id in range(NUM_EPOCHS):
        try:
            # Do the gradient steps and compute training loss.
            tr_loss = 0
            for batch_id in range(BATCHES_PER_EPOCH):
                x_tr, y_tr = gen_batch_simple(
                    batch_size=BATCH_SIZE, seq_len=SEQ_LEN,
                    min_seq_len=SEQ_LEN, which='train', log=True)
                tr_loss += train(x_tr, y_tr, LEARNING_RATE)

            # Compute the validation loss.
            val_loss = 0
            for batch_id in range(100):
                val_loss += validate(x_tst[batch_id*100:(batch_id+1)*100],
                                     y_tst[batch_id*100:(batch_id+1)*100])

            # Save the loss.
            loss_history[0, epoch_id] = tr_loss/float(
                BATCHES_PER_EPOCH*BATCH_SIZE)
            loss_history[1, epoch_id] = val_loss/10000.

            # If the network is doing well on validation data, save it.
            if np.isfinite(val_loss) and val_loss < best_loss:
                joblib.dump((net, loss_history), SAVE_FNAME)
                best_loss = val_loss

            print('Epoch {}. Learning rate {}.\nValid NLL {}. Train NLL {}'.
                  format(epoch_id, LEARNING_RATE, val_loss/10000.,
                         tr_loss/float(BATCHES_PER_EPOCH*BATCH_SIZE)))

        except KeyboardInterrupt:
            print 'Training interrupted.'
            break

if __name__ == "__main__":
    train_probnn()
