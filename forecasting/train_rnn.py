import sys
import joblib
import numpy as np
import theano 
from theano import tensor as T
import lasagne

from helpers import gen_batch

LOAD_SAVED=False
WATER=False
NUM_EPOCHS=1000
BATCHES_PER_EPOCH=32
BATCH_SIZE=128
SEQ_LEN=32
load_fname = 'net_params/rnn33'
save_fname = 'net_params/rnn33'

if LOAD_SAVED:
    net = joblib.load(load_fname)
else:
    # Assemble the network.
    gate_parameters = lasagne.layers.recurrent.Gate(W_in=lasagne.init.Orthogonal(), 
        W_hid=lasagne.init.Orthogonal(), b=lasagne.init.Constant(0.))
    cell_parameters = lasagne.layers.recurrent.Gate(W_in=lasagne.init.Orthogonal(), 
        W_hid=lasagne.init.Orthogonal(), W_cell=None, b=lasagne.init.Constant(0.), 
            nonlinearity=lasagne.nonlinearities.tanh)

    nonlin = lasagne.nonlinearities.tanh
    l0 = lasagne.layers.InputLayer(shape=(None, SEQ_LEN, 1+int(WATER)))
    lmask = lasagne.layers.InputLayer(shape=(None, SEQ_LEN))
    l1 = lasagne.layers.LSTMLayer(l0, 512, grad_clipping=100, 
                                  nonlinearity=nonlin, learn_init=True, mask_input=lmask,
                                  ingate=gate_parameters, forgetgate=gate_parameters,
                                  cell=cell_parameters, outgate=gate_parameters)
    l2 = lasagne.layers.LSTMLayer(l1, 512, grad_clipping=100, 
                                  nonlinearity=nonlin, learn_init=True,
                                  ingate=gate_parameters, forgetgate=gate_parameters,
                                  cell=cell_parameters, outgate=gate_parameters)
    l3 = lasagne.layers.LSTMLayer(l2, 512, grad_clipping=100,
                                  nonlinearity=nonlin, learn_init=True)
    l4 = lasagne.layers.SliceLayer(l3, -1, 1)
    net = lasagne.layers.DenseLayer(l4, num_units=1+int(WATER), 
        nonlinearity=lasagne.nonlinearities.identity)

# Assemble the training algorithm.
print('Compiling the theano net...')
input_var = lasagne.layers.get_all_layers(net)[0].input_var
mask_var = lasagne.layers.get_all_layers(net)[1].input_var
target_var = T.vector('target_output')
tr_cost = T.mean((lasagne.layers.get_output(net).flatten()-target_var)**2)
val_cost = T.mean((lasagne.layers.get_output(net, deterministic=True).flatten()-target_var)**2)
params = lasagne.layers.get_all_params(net)
updates = lasagne.updates.adam(tr_cost, params, learning_rate=.001)
#updates = lasagne.updates.rmsprop(tr_cost, params)#, learning_rate=.0001, momentum=.9)
train = theano.function([input_var, target_var, mask_var], tr_cost, updates=updates, allow_input_downcast=True)
validate = theano.function([input_var, target_var, mask_var], val_cost, allow_input_downcast=True)

# Train the network.
print('Done. Training...')
X_tst, y_tst, mask_tst = gen_batch(batch_size=BATCH_SIZE*BATCHES_PER_EPOCH, 
                                   seq_len=SEQ_LEN, min_seq_len=SEQ_LEN, which='test', water=WATER)
best_loss = np.inf
for epoch_id in range(NUM_EPOCHS):
    try:
        tr_loss = 0
        for batch_id in range(BATCHES_PER_EPOCH):
            X_tr, y_tr, mask_tr = gen_batch(batch_size=BATCH_SIZE, 
                seq_len=SEQ_LEN, min_seq_len=SEQ_LEN, which='train', water=WATER)
            tr_loss += train(X_tr, y_tr.flatten(), mask_tr)
        val_loss = validate(X_tst, y_tst.flatten(), mask_tst)
        if np.isfinite(val_loss) and val_loss < best_loss:
            joblib.dump(net, save_fname)
            best_loss = val_loss
        print('Epoch {}. Valid MSE {}. Train MSE {}'.format(epoch_id, np.sqrt(val_loss/float(BATCHES_PER_EPOCH)), np.sqrt(tr_loss/BATCHES_PER_EPOCH)))
    except KeyboardInterrupt:
        print('Training interrupted.')
        break
