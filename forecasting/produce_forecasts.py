""" Predict well behavior for all the test wells in given state. """

import sys
import calendar
from datetime import datetime
import MySQLdb
import joblib
import numpy as np
import theano
import lasagne
from helpers import predict_pnn
from helpers import fetch_well
import helpers
from terraai_preprocessing.preprocessing import pp_main
from terraai_preprocessing.preprocessing import pp_helpers


TRAIN_END = datetime.date(datetime.strptime('2009-1-31', '%Y-%m-%d'))
PRED_LEN = 7*12
SCHEMA_IN = 'public_data'
SCHEMA_OUT = 'results'


### THIS CODE IS COPYPASTED FROM REZAs SCRIPT. TODO: IMPORT REZAS CODE INSTEAD!!! --------------------------------------------
def add_months(sourcedatestr, months):
    if type(sourcedatestr) == str: 
        sourcedate = datetime.strptime(sourcedatestr , '%Y-%m-%d').date() 
    else:
        sourcedate = sourcedatestr
    
    month = [ sourcedate.month - 1 + months[i] for i in xrange(len(months)) ]
    year  = [int(sourcedate.year + x / 12 ) for x in month]
    month = [ month[i] % 12 + 1 for i in xrange(len(month)) ]
    day   = [min(sourcedate.day , calendar.monthrange(year[i],month[i])[1]) for i in xrange(len(year)) ]
    
    
    day        = [calendar.monthrange(year[i],month[i])[1] for i in range(len(year))] # get the end of month day number date
    datestring = [(str(year[i])+"-"+str(month[i])+"-"+str(day[i])) for i in range(len(year))]   
    dates      = [datetime.strptime(datestring[i] , '%Y-%m-%d').date() for i in range(len(datestring))]    
    return datestring , dates
###---------------------------------------------------------------------------------------------------------------------------

def load_nn(nn_file):
    """
    Load and compile the neural net that we'll use to make predictions.

    Args:
      nn_file: A (string) name of the pkl file 
               that contains a joblib-saved neural net.

    Returns:
      predictor: Compiled neural net.
      in_dim: Length (in months) of sequences to be fed to the network.

    """
    net, _ = joblib.load(nn_file)
    in_dim = lasagne.layers.get_all_layers(net)[1].input_shape[-1]
    invar = lasagne.layers.get_all_layers(net)[0].input_var
    return (theano.function(
        [invar], lasagne.layers.get_output(net, deterministic=True),
        allow_input_downcast=True),
            in_dim)

if __name__ == "__main__":
    """ argv[1] - file containing the net weights.
        argv[2] - state to predict (integer).
        argv[3] - name of results table to save to.
        argv[4] - bias param (integer, usually 0 or 10).
        argv[5] - whether to predict the expected (1) or max-likelihood (0) prod.
    """
    print 'Loading and compiling the network...'
    predictor, min_months = load_nn(sys.argv[1])
    state_id = sys.argv[2]
    out_table = sys.argv[3]
    bias = float(sys.argv[4])
    expected = bool(sys.argv[5])
    _, CURSOR_IN = pp_helpers.establish_db_conn()
    well_ids = pp_helpers.get_well_ids(CURSOR_IN, ["API_prod"], state_id)
    
    newtable = True
    for well_id_id, well_id in enumerate(well_ids):
        data, _, _, _ = pp_main.start_preprocess(
            well_ids=[well_id], mp_flag=False, 
            verbosity=False, time_trsh=2, static_attr=False)
        if len(data) == 0:
            continue
        else:
            data = data[0]

        # Check if this is a test well (i.e. well that's producing on 
        # the TEST_START date.)
        test_start_id = np.where(data['report_date_mo']==TRAIN_END)[0]
        if test_start_id.size == 0:
            continue
        else:
            test_start_id = test_start_id[0]
        if test_start_id == 0:
            continue
        oil = np.array(np.asarray(data['oil_pd']) * 
                       np.asarray(data['prod_days']) + 1).astype(float)
        x_in = np.log(oil[max(0, test_start_id-min_months):test_start_id]+1)
        true_len = float(x_in.size)
        x_in = np.tile(x_in, (
            1, int(np.ceil(min_months / true_len))))[0, -min_months:]

        pred, _ = list(predict_pnn(
            predictor, x_in.reshape(1, 1, min_months), 
            num_steps=PRED_LEN, n_samples=10000, bias=bias, 
            density_grid=np.linspace(0,10,1000), expected=expected))
        pred = np.exp(pred.flatten()[-PRED_LEN:])-1

        x_inorig = np.array(oil[max(0, test_start_id-min_months):test_start_id])
        x_inorig = np.concatenate(
            [np.zeros(min_months-x_inorig.size), x_inorig])
        x_outorig = np.array(oil[test_start_id:test_start_id+PRED_LEN])
        x_outorig = np.concatenate(
            [x_outorig, np.zeros(PRED_LEN-x_outorig.size)])

	pred[np.isinf(pred)] = 0.
	pred[np.isnan(pred)] = 0.
	
        sys.stdout.write('\rProcessing well {}, {}/{}.'.format(
            well_id, well_id_id, len(well_ids)))
        sys.stdout.flush()

        output = {}
        output['API_prod'] = (np.ones(min_months+PRED_LEN, dtype=long) * 
                              data['API_prod'][0]).tolist()
        output['report_date_mo'] = add_months(str(TRAIN_END), 
                                              range(-min_months, PRED_LEN))[1]
        output['oil_pd'] = np.concatenate([x_inorig, x_outorig]).tolist()
        output['oil_pd_fore_mean'] = np.concatenate(
            [x_inorig, pred.flatten()]).tolist()
        output['oil_pd_fore_p10'] = np.zeros(min_months+PRED_LEN).tolist()
        output['oil_pd_fore_p90'] = np.zeros(min_months+PRED_LEN).tolist()
        output['oil_pd_fore_std'] = np.zeros(min_months+PRED_LEN).tolist()
        output['prod_days'] = np.zeros(min_months+PRED_LEN).tolist()
        output['wtr_pd'] = np.zeros(min_months+PRED_LEN).tolist()
        output['wtr_pd_fore_mean'] = np.zeros(min_months+PRED_LEN).tolist()
        output['wtr_pd_fore_p10'] = np.zeros(min_months+PRED_LEN).tolist()
        output['wtr_pd_fore_p90'] = np.zeros(min_months+PRED_LEN).tolist()
        output['wtr_pd_fore_std'] = np.zeros(min_months+PRED_LEN).tolist()

        conn, cursor = pp_main.establish_db_conn()
        if newtable:
            newtable = False
            pp_main.write_DB(conn, cursor, [output], 
                             verbosity=False, schema='results',
                             table_name=out_table, drop_table=True)
        else:
            pp_main.write_DB(conn, cursor, [output], 
                             verbosity=False, schema='results',
                             table_name=out_table, drop_table=False)
