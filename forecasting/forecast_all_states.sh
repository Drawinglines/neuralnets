#!/bin/bash
STATE=0
BIAS=0
EXPECTED=10
while [ $STATE -lt 51 ]; do
    python produce_forecasts.py net_params/2gauss_net $STATE fore_NN_2gauss_net_expected${EXPECTED}_bias${BIAS}_$STATE $BIAS $EXPECTED
    let STATE=STATE+1
done
