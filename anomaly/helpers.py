from __future__ import division
from collections import defaultdict
import MySQLdb
import numpy as np
from datetime import datetime, date
import cPickle as pk
import pdb

cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'aamir'}
initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')

def diff_month(d1):
    try:
        return (d1.year - initDate.year)*12 + d1.month - initDate.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d1)
        return np.NaN

def add_months(months):
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,1)

def diff2Dates(din):
    return 

def mynum(func, a):
    ''' This function is only a way to get around the unexpected data.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
    	# pdb.set_trace()
        print('Expected float, got {} instead'.format(type(a)))
        return 0

def readFromDBsimple(wellIDs):
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = "SELECT CF_Flag,report_date_mo,LastProdDay FROM aamir.Taha_NDK_CatFail WHERE API_prod in ({}) and CF_Flag!=2".format(','.join([str(x) for x in wellIDs]))
    cursor.execute(query)
    row = cursor.fetchall()
    ltime, obs = list(), list()
    for (i, item) in enumerate(row):
        init  = min([datetime.strptime(x, '%Y-%m-%d') for x in item[1].split(',')])
        final = datetime.strptime(item[2], '%Y-%m-%d')
        ltime.append(diff_month(final)-diff_month(init))
        obs.append(int(item[0])==1)
    cursor.close()
    conn.close()

    return np.array(ltime), np.array(obs)

def readFromDB():
    # Establish the connection
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = "SELECT * FROM aamir.Taha_NDK_CatFail WHERE `CF_Flag`!=2"
    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    # pdb.set_trace()
    # id:0, date:1, proddays:2, oil:3, water:4, oprice:5, lat:7, long:8, lastday:11, CF:12
    for (i, item) in enumerate(row):
        # Exclude all-zero rows
        oil = np.array([mynum(float,x) for x in item[3].split(',')])
        if oil.sum() == 0:  continue
        
        dates  = np.array([diff_month(datetime.strptime(x, '%Y-%m-%d')) for x in item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float,x) for x in item[4].split(',')])[index]
        days = np.array([mynum(int,x) for x in item[2].split(',')])[index]
        try:
        	price  = np.array([mynum(float,x) for x in item[5].split(',')])[index]
        except:
        	pdb.set_trace()

        data['padID'].append( item[0] )
        data['dates'].append( dates )
        data['oil'].append( oil[index] )
        data['water'].append( water )
        data['proddays'].append( days )
        data['last'].append( diff_month(datetime.strptime(item[11], '%Y-%m-%d')) )
        data['price'].append( price )
        data['latitude'].append( item[7] )
        data['longitude'].append( item[8] )
        data['CF'].append(mynum(int, item[12]))
        if i%1000 == 0: print i     # This is a counter, because it takes a long time.

    with open('NDKdataCF.p', 'wb') as f:
        pk.dump(data, f)

    cursor.close()
    conn.close()
##################### Data Loader #####################
def formatDataSurvival():
    with open('NDKdataCF.p', 'rb') as f:
        data = pk.load(f)
    # pdb.set_trace()
    X, y, s = list(), list(), list()
    for i in range(len(data['padID'])):
        if data['CF'][i]!=3 and data['CF'][i]!=1: continue
        # Select data before the CF
        index = np.where(data['dates'][i] < data['last'][i])[0]
        Xtemp = np.vstack((data['oil'][i][index], data['water'][i][index], data['proddays'][i][index], data['price'][i][index]))
        Xtemp = np.vstack((np.log10(1+Xtemp), data['dates'][i][index]/1000))
        stemp = np.array([data['latitude'][i], data['longitude'][i]])/100
        ytemp = data['last'][i] - np.array(data['dates'][i][index])
        censor = 1 if data['CF'][i]==3 else 0
        ytemp = np.vstack( (ytemp, censor*np.ones((ytemp.shape[0],))) )

        X.append(Xtemp)
        y.append(ytemp)
        s.append(stemp)

    with open('NDKdataCFformatted.p', 'wb') as f:
        pk.dump({'X':X, 'y':y, 's':s}, f)

if __name__ == "__main__":
    # readFromDB()
    formatDataSurvival()