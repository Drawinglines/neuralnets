from __future__ import division
from keras.models import Sequential  
from keras.layers.core import Dense, TimeDistributedDense, Masking, Dropout  
from keras.layers.recurrent import GRU
from os import fsync
import numpy as np
import helpers as hlp 
import cPickle as pk
import pdb

inDim = 7
nHid = 10
batchSize = 200
doRate = 0.5
nEpoch = 100
tscale = 120
Tmax = 200

# Load the data globally
with open('NDKdataCFformatted.p', 'rb') as f:
    data = pk.load(f)
    X = data['X']
    y = data['y']
    s = data['s']

# pdb.set_trace()

def getIndexes(ind):
    n = ind.shape[0]
    nBatch = int(n/batchSize)+1
    index = [ind[range(i*batchSize, min((i+1)*batchSize, n))] for i in range(nBatch)]
    return index

def partition(n, testRatio=0.15,validRatio=0):
    np.random.seed(0)    # Reproducability
    ind = np.random.permutation(n)
    nTest = int(testRatio*n)
    nValid = int(validRatio*n)
    return ind[nTest+nValid:],ind[0:nTest],ind[nTest:nTest+nValid]      # Train, Test, Validation

def feedBatch(ind):
    ind = [ii for ii in ind if X[ii].shape[1]>0]
    ff = list()
    ll = list()
    for (i, jn) in enumerate(ind):
        if y[jn][1,0] == 0: continue
        T = X[jn].shape[1]
        X[jn][~np.isfinite(X[jn])] = 0   ########Careful
        srep = np.tile(np.array(s[jn])[:, None], (1, T))
        ff.append( np.vstack((X[jn], srep)).T )
        ll.append( np.vstack(y[jn][0,:]) ) 
    return np.concatenate(ff), np.concatenate(ll)

def buildModel():
    model = Sequential()  
    model.add(Dense(nHid, input_dim=inDim, activation='relu'))
    model.add(Dropout(doRate))
    model.add(Dense(nHid, activation='relu'))
    model.add(Dropout(doRate))
    model.add(Dense(1, activation='linear'))  
    model.compile(loss="mape", optimizer="adadelta") 
    print "Model compiled."
    return model

if __name__ == "__main__":
    indTrain, indTest, indValid = partition(len(X))
    model = buildModel()
    X, y = feedBatch(indTrain)
    model.fit(X, y, nb_epoch=nEpoch, validation_split=0.15)
    pdb.set_trace()
    model.save_weights('./weights/survival.wei', overwrite=True)
