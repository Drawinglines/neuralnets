from __future__ import division
import warnings
from collections import defaultdict
from datetime import datetime, date
import cPickle as pk
import pdb
import MySQLdb
import numpy as np
from tqdm import tqdm

warnings.filterwarnings("ignore")  # Not a very brilliant idea!

cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'aamir'}
initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')

def diff_month(d1):
    try:
        return (d1.year - initDate.year)*12 + d1.month - initDate.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d1)
        return np.NaN

def add_months(months):
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,1)

def diff2Dates(din):
    return 

def mynum(func, a):
    ''' This function is only a way to get around the unexpected data.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
    	# pdb.set_trace()
        print('Expected float, got {} instead'.format(type(a)))
        return 0

def handleMissing(dates, oil, water, days, price):
    """
    This function handles the missing data points by inserting zeros in the missing months.
    """
    new_dates = np.arange(min(dates), max(dates)+1) 
    new_oil, new_water, new_days, new_price = [np.zeros(new_dates.shape) for _ in range(4)]
    new_oil[dates-min(dates)] = oil
    new_water[dates-min(dates)] = water
    new_days[dates-min(dates)] = days
    new_price[dates-min(dates)] = price
    return new_dates, new_oil, new_water, new_days, new_price

def readFromDBsimple(wellIDs):
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = "SELECT CF_Flag,report_date_mo,LastProdDay FROM aamir.Taha_NDK_CatFail WHERE API_prod in ({}) and CF_Flag!=2".format(','.join([str(x) for x in wellIDs]))
    cursor.execute(query)
    row = cursor.fetchall()
    ltime, obs = list(), list()
    for (i, item) in enumerate(row):
        init  = min([datetime.strptime(x, '%Y-%m-%d') for x in item[1].split(',')])
        final = datetime.strptime(item[2], '%Y-%m-%d')
        ltime.append(diff_month(final)-diff_month(init))
        obs.append(int(item[0])==1)
    cursor.close()
    conn.close()

    return np.array(ltime), np.array(obs)

def readFromDB():
    # Establish the connection
    conn = MySQLdb.connect(cnx['host'], cnx['user'], cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = """SELECT api, dates, proddays, oil, water, price, lat, longi, EndDate, CF_flag FROM public_data.big_cf;"""
    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    counter = 0
    # id:0, date:1, proddays:2, oil:3, water:4, oprice:5, lat:7, long:8, lastday:11, CF:12
    for (i, item) in tqdm(enumerate(row)):
        # Exclude all-zero rows
        oil = np.array([mynum(float, x) for x in item[3].split(',')])
        if oil.sum() == 0:  
            continue

        dates  = np.array([diff_month(datetime.strptime(x, '%Y-%m-%d')) for x in item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        try:
            water = np.array([mynum(float, x) for x in item[4].split(',')])[index]
            days = np.array([mynum(int, x) for x in item[2].split(',')])[index]
            price  = np.array([mynum(float, x) for x in item[5].split(',')])[index]
        except:
            counter += 1
            print "Error {}".format(counter)
            pdb.set_trace()
        dates, oil, water, days, price = handleMissing(dates, oil[index], water, days, price)

        data['padID'].append(item[0])
        data['dates'].append(dates)
        data['oil'].append(oil)
        data['water'].append(water)
        data['proddays'].append(days)
        data['last'].append(diff_month(item[8]))
        data['price'].append(price)
        data['latitude'].append(mynum(float, item[6]))
        data['longitude'].append(mynum(float, item[7]))
        data['CF'].append(mynum(int, item[9]))

    cursor.close()
    conn.close()
    return data
##################### Data Loader #####################
def format_data_survival():
    print "Formatting for survival analysis."
    data = readFromDB()
    X, y, stat, cens, apis, start = list(), list(), list(), list(), list(), list()
    for i in tqdm(range(len(data['padID']))):
        if (data['CF'][i] != 3) and (data['CF'][i] != 1):
            continue
        # Select data before the CF
        apis.append(data['padID'][i])
        start.append(data['dates'][i][0])
        # pdb.set_trace()
        index = np.where(data['dates'][i] < data['last'][i])[0]
        Xtemp = np.vstack((data['oil'][i][index], data['water'][i][index], data['proddays'][i][index], data['price'][i][index]))
        Xtemp = np.vstack((np.log10(1+Xtemp), data['dates'][i][index]/1000))
        stemp = np.array([data['latitude'][i], data['longitude'][i]])/100
        ytemp = data['last'][i] - np.array(data['dates'][i][index])
        censor = 1 if data['CF'][i] == 3 else 0

        X.append(Xtemp)
        y.append(ytemp)
        stat.append(stemp)
        cens.append(censor)

    with open('bigdataCFformatted.p', 'wb') as f:
        pk.dump({'X':X, 'y':y, 'stat':stat, 'cens':cens, 'start':start, 'apis':apis}, f)
    #joblib.dump({'X':X, 'y':y, 'stat':stat, 'cens':cens}, 'data/NDKdataCFformatted')

if __name__ == "__main__":
    # readFromDB()
    format_data_survival()
