from __future__ import print_function, division

from collections import defaultdict
from datetime import datetime, date
from calendar import monthrange
import cPickle as pk
import pdb
import MySQLdb
import numpy as np
from tqdm import tqdm

cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'results'}
initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')


def add_months(months):
    """
    Returns the date for several months after the initDate. 
    """
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,monthrange(year,month)[1])  # Reporting on the last day of the month

def write2db(data, name):
    """
    This function writes the data formatted and returned by convertData() into the database. 
    """
    # Establish the connection  
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    
    # Drop table if exists
    query = "DROP TABLE IF EXISTS {};".format(name)
    cursor.execute(query)

    query = """CREATE TABLE {} (API_prod BIGINT, report_date_mo DATE, hazard DOUBLE, probability DOUBLE);""".format(name)
    cursor.execute(query)

    # Insert the rows
    values = list()
    for (i, well) in enumerate(data['padID']):
        for j in range(len(data['date'][i])):
            values.append( "('{}','{}',{},{})".format(well, \
                data['date'][i][j], data['hazard'][i][j], data['prob'][i][j]))

    stepSize = 1000    # Doing the queries in batches of size 1000
    nBatch = int(np.ceil(len(values)/stepSize))
    ind = [min(x*stepSize,len(values)) for x in range(nBatch+1)]
    for i in tqdm(range(nBatch)):
        query = 'INSERT INTO {} VALUES {};'.format(name,','.join(values[ind[i]:ind[i+1]]))
        query = query.replace('nan', 'NULL')
        query = query.replace('inf', 'NULL')
        cursor.execute(query)

    conn.commit()
    cursor.close()
    conn.close()

def format_output(apis, start_date, pred, mask):
    data = defaultdict(list)
    for (i, wellid) in enumerate(apis):
        hazards = np.exp(pred[i, :])[np.where(mask[i, :] == 1)[0]]
        dates = np.arange(start_date[i], start_date[i]+hazards.shape[0]).tolist()
        data['padID'].append(wellid)
        data['date'].append([add_months(x) for x in dates])
        data['hazard'].append(hazards)
        data['prob'].append(1-np.exp(-hazards))
    return data

def main():
    with open('NDKdataCFformatted.p', 'rb') as f:
        data = pk.load(f)
        apis = data['apis']
        start_date = data['start']
    print("Loaded the data.")

    with open('NDKresults.p', 'rb') as f:
        results = pk.load(f)
        inds = results['inds']
        pred = results['pred']
        mask = results['mask']
    print("Loaded the results.")

    apis = np.array(apis)[inds]
    start_date = np.array(start_date)[inds]
    data = format_output(apis, start_date, pred[:, :, 0], mask)
    write2db(data, 'CF_NDK_3layer_500neurons')

if __name__ == "__main__":
    # readFromDB()
    main()
