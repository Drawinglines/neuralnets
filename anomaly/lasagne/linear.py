#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Recurrent network example.  Trains a bidirectional vanilla RNN to output the
sum of two numbers in a sequence of random numbers sampled uniformly from
[0, 1] based on a separate marker sequence.
'''

from __future__ import print_function, division

import numpy as np
import theano
import theano.tensor as T
import lasagne
import cPickle as pk
import time
import joblib

# Min/max sequence length
MIN_LENGTH = 50
MAX_LENGTH = Tmax = 55
# Number of units in the hidden (recurrent) layer
N_HIDDEN = 500
# Number of training sequences in each batch
N_BATCH = 100
# Optimization learning rate
LEARNING_RATE = .001
# All gradients above this will be clipped
GRAD_CLIP = 100
# How often should we check the output?
EPOCH_SIZE = 100
# Number of epochs to train the net
NUM_EPOCHS = 500

batchSize = 100 
tscale = 120
weigth_name = 'weights/rnn'

# Load the data globally
with open('NDKdataCFformatted.p', 'rb') as f:
    data = pk.load(f)
    X = data['X']
    y = data['y']
    stat = data['stat']
    censor = data['cens']

def getIndexes(ind, shuffle=False):
    n = ind.shape[0]
    if shuffle:   ind = ind[np.random.permutation(n)]
    nBatch = int(n/batchSize)  # We ignore the last batch, because its size is different.
    index = [ind[range(i*batchSize, min((i+1)*batchSize, n))] for i in range(nBatch)]
    return index

def partition(n=len(X), testRatio=0.05, validRatio=0.15):
    np.random.seed(0)    # Reproducability
    ind = np.random.permutation(n)
    nTest = int(testRatio*n)
    nValid = int(validRatio*n)
    return ind[nTest+nValid:],ind[0:nTest],ind[nTest:nTest+nValid]      # Train, Test, Validation

def feedBatch(ind):
    ind = [ii for ii in ind if X[ii].shape[1]>0]
    n, dd = len(ind), X[0].shape[0]+len(stat[0])
    # n, Tmax, dd = len(ind), max([X[ii].shape[1] for ii in ind]), X[0].shape[0]+len(s[0])
    features = np.zeros((n, Tmax, dd))
    labels = np.zeros((n, Tmax, 1))
    fail = np.zeros((n, Tmax, 1))
    mask = np.zeros((n, Tmax))
    for (i, jn) in enumerate(ind):
        T = min(X[jn].shape[1], Tmax)
        X[jn][~np.isfinite(X[jn])] = 0   ########Careful
        srep = np.tile(np.array(stat[jn])[:, None], (1, T))
        features[i, -T:, :] = np.vstack((X[jn][:, -T:], srep)).T
        labels[i, -T:, 0] = y[jn][-T:]/tscale
        fail[i, -T:, 0] = (1-censor[jn])*np.ones((T,))
        mask[i, -T:] = 1
    # Flattening is because of Lasagne's syntax
    return features, labels.flatten(), fail.flatten(), mask

def evaluate(targets, failure, preds, mask):
    # import pdb; pdb.set_trace()
    # mape = (abs( (targets-np.exp(-preds.flatten()))/targets )*mask.flatten())*failure
    mape = (abs( (targets-preds.flatten())/targets )*mask.flatten())*failure
    mape[~np.isfinite(mape)] = 0
    return sum(mape), sum(mask.flatten())

def main(num_epochs=NUM_EPOCHS):
    print("Building network ...")
    # First, we build the network, starting with an input layer
    # Recurrent layers expect input of shape
    # (batch size, max sequence length, number of features)
    l_in = lasagne.layers.InputLayer(shape=(N_BATCH, MAX_LENGTH, 7))
    # The network also needs a way to provide a mask for each sequence.  We'll
    # use a separate input layer for that.  Since the mask only determines
    # which indices are part of the sequence for each batch entry, they are
    # supplied as matrices of dimensionality (N_BATCH, MAX_LENGTH)
    l_mask = lasagne.layers.InputLayer(shape=(N_BATCH, MAX_LENGTH))
    # We're using a bidirectional network, which means we will combine two
    # RecurrentLayers, one with the backwards=True keyword argument.
    # Setting a value for grad_clipping will clip the gradients in the layer
    # Setting only_return_final=True makes the layers only return their output
    # for the final time step, which is all we need for this task
    l_forward1 = lasagne.layers.GRULayer(
        l_in, N_HIDDEN, mask_input=l_mask, grad_clipping=GRAD_CLIP)
    l_forward2 = lasagne.layers.GRULayer(
        l_forward1, N_HIDDEN, mask_input=l_mask, grad_clipping=GRAD_CLIP)
    l_forward3 = lasagne.layers.GRULayer(
        l_forward2, N_HIDDEN, mask_input=l_mask, grad_clipping=GRAD_CLIP)
    # Our output layer is a simple dense connection, with 1 output unit

    l_reshape = lasagne.layers.ReshapeLayer(l_forward3, (-1, N_HIDDEN))
    l_dense = lasagne.layers.DenseLayer(
        l_reshape, num_units=1, nonlinearity=lasagne.nonlinearities.linear)
    net = lasagne.layers.ReshapeLayer(l_dense, (N_BATCH, MAX_LENGTH, 1))

    target_values = T.vector('target_output')
    failure_values = T.vector('failure')

    # lasagne.layers.get_output produces a variable for the output of the net
    network_output = lasagne.layers.get_output(net)
    # The network output will have shape (n_batch, 1); let's flatten to get a
    # 1-dimensional vector of predicted values
    predicted_values = network_output.flatten()
    # Our cost will be mean-squared error
    cost = T.mean( abs(predicted_values - target_values)*failure_values)
    # Retrieve all parameters from the network
    all_params = lasagne.layers.get_all_params(net)
    # Compute SGD updates for training
    print("Computing updates ...")
    updates = lasagne.updates.adagrad(cost, all_params, LEARNING_RATE)
    # Theano functions for training and computing cost
    print("Compiling functions ...")
    train = theano.function([l_in.input_var, target_values, failure_values, l_mask.input_var],
                            cost, updates=updates, allow_input_downcast=True)
    compute_cost = theano.function(
        [l_in.input_var, target_values, failure_values, l_mask.input_var], cost, allow_input_downcast=True)

    # Finally, launch the training loop.
    print("Starting training...")
    indTrain, indTest, indValid = partition()
    indValidBatches = getIndexes(indValid)
    # We iterate over epochs:
    val_best = np.inf 
    for epoch in range(num_epochs):
        # In each epoch, we do a full pass over the training data:
        train_err = 0
        start_time = time.time()
        indTrainBatches = getIndexes(indTrain, True) # Shuffle the training set
        for batch in indTrainBatches:
            inputs, targets, failure, mask = feedBatch(batch)
            train_err += train(inputs, targets, failure, mask)

        val_err = 0
        for batch in indValidBatches:
            inputs, targets, failure, mask = feedBatch(batch)
            val_err += compute_cost(inputs, targets, failure, mask)

        # Then we print the results for this epoch:
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / len(indTrainBatches)))
        print("  validation loss:\t\t{:.6f}".format(val_err / len(indValidBatches)))

        # Saving the best model
        if np.isfinite(val_err) and val_err < val_best:
            val_best = val_err
            bestNet = net
            # joblib.dump(net, weigth_name)

    # Testing
    net_output = lasagne.layers.get_output(bestNet)
    out_fun = theano.function([l_in.input_var, l_mask.input_var], net_output, allow_input_downcast=True)
    indTestBatches = getIndexes(indTest)
    testErr, numSamples = 0, 0
    for batch in indValidBatches:
        inputs, targets, failure, mask = feedBatch(batch)
        preds = out_fun(inputs, mask)
        err, num = evaluate(targets, failure, preds, mask)
        testErr += err 
        numSamples += num 
    print(testErr/numSamples)

if __name__ == '__main__':
    main()
