import numpy as np
import theano
import theano.tensor as T
import lasagne

# Min/max sequence length
MIN_LENGTH = 50
MAX_LENGTH = 55
# Number of units in the hidden (recurrent) layer
N_HIDDEN = 100
# Number of training sequences in each batch
N_BATCH = 100
# All gradients above this will be clipped
GRAD_CLIP = 100

print("Building network ...")
l_in = lasagne.layers.InputLayer(shape=(N_BATCH, MAX_LENGTH, 2))

l_mask = lasagne.layers.InputLayer(shape=(N_BATCH, MAX_LENGTH))

l_forward = lasagne.layers.GRULayer(
    l_in, N_HIDDEN, mask_input=l_mask, grad_clipping=GRAD_CLIP)
# Our output layer is a simple dense connection, with 1 output unit
l_reshape = lasagne.layers.ReshapeLayer(l_forward, (-1, N_HIDDEN))
l_dense = lasagne.layers.DenseLayer(
    l_reshape, num_units=1, nonlinearity=lasagne.nonlinearities.linear)
l_out = lasagne.layers.ReshapeLayer(l_dense, (N_BATCH, MAX_LENGTH, 1))

# lasagne.layers.get_output produces a variable for the output of the net
network_output = lasagne.layers.get_output(l_out)

output = theano.function(
    [l_in.input_var, l_mask.input_var], network_output)

# inputs
inn = np.random.random((N_BATCH, MAX_LENGTH, 2))
mask =np.ones((N_BATCH, MAX_LENGTH))

import pdb; pdb.set_trace()
xx = output(inn, mask)
print("Shape of xx: {0}".format(xx.shape))
print("Shape of inn: {}".format(inn.shape))
