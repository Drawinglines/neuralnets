-- Joining big_prod and oil_price
CREATE TABLE big_price AS
SELECT big_prod.`API_prod` as api,
       big_prod.`state_API` as state,
       big_prod.`report_date` as dates,
       big_prod.`prod_days` as proddays,
       big_prod.oil as oil,
       big_prod.`water` as water,
       oil_price_monthly.`price` as price
FROM big_prod
LEFT JOIN oil_price_monthly
ON big_prod.`report_date`=oil_price_monthly.`Monthly_date`;

-- Now Group-concat and get time series
CREATE TABLE big_price_ts AS
SELECT api, 
       state, 
       group_concat(dates) as dates, 
       group_concat(IFNULL(proddays, 0)) as proddays, 
       group_concat(IFNULL(oil, 0)) as oil, 
       group_concat(IFNULL(water, 0)) as water, 
       group_concat(IFNULL(price, 0)) as price 
FROM `big_price` GROUP BY api ORDER BY api;

-- Now join the stuff from the CF table
CREATE TABLE big_price_ts_cf AS
SELECT big_price_ts.api,
       big_price_ts.state,
       big_price_ts.dates,
       big_price_ts.proddays,
       big_price_ts.oil,
       big_price_ts.water,
       big_price_ts.price,
       all_cf_flag.`EndDate`,
       all_cf_flag.`CF_flag`
FROM big_price_ts
INNER JOIN all_cf_flag 
ON big_price_ts.api=all_cf_flag.`API`
WHERE all_cf_flag.CF_flag!=2;

-- Finally, join the static attributes from big_master
CREATE TABLE big_cf AS
SELECT big_price_ts_cf.api,
       big_price_ts_cf.dates,
       big_price_ts_cf.proddays,
       big_price_ts_cf.oil,
       big_price_ts_cf.water,
       big_price_ts_cf.price,
       big_master.`lat`,
       big_master.`longi`,
       big_price_ts_cf.`EndDate`,
       big_price_ts_cf.`CF_flag`
FROM big_price_ts_cf
INNER JOIN big_master 
ON big_price_ts_cf.api=big_master.`API_master`;