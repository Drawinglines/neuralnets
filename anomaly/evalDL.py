from __future__ import division
import numpy as np
import helpers as hlp 
import linear as bc
import cPickle as pk
import pdb
import scipy.io as sio

tTest = 36

with open('NDKdataCFformatted.p', 'rb') as f:
    data = pk.load(f)
    X = data['X']
    y = data['y']
    s = data['s']

def partition(n, testRatio=0.15,validRatio=0.1):
    np.random.seed(0)    # Reproducability
    ind = np.random.permutation(n)
    nTest = int(testRatio*n)
    nValid = int(validRatio*n)
    return ind[nTest+nValid:],ind[0:nTest],ind[nTest:nTest+nValid]      # Train, Test, Validation

def feedBatch(ind):
    ind = [ii for ii in ind if X[ii].shape[1]>0]
    ff = list()
    ll = list()
    for (i, jn) in enumerate(ind):
        if y[jn][1,0] == 0: continue
        T = X[jn].shape[1]
        X[jn][~np.isfinite(X[jn])] = 0   ########Careful
        srep = np.tile(np.array(s[jn])[:, None], (1, T))
        ff.append( np.vstack((X[jn], srep)).T )
        ll.append( np.vstack(y[jn][0,:]) ) 
    return np.concatenate(ff), np.concatenate(ll)

def evalRM(ytrue, ypred):
    pdb.set_trace()
    r2 = 1- ((ytrue-ypred)**2).sum()/((ytrue-ytrue.mean())**2).sum()
    mape = (abs( (ytrue-ypred)/ytrue )).mean()
    return r2, mape

if __name__ == "__main__":
    # Model fitting
    indTrain, indTest, indValid = partition(len(X))
    inds = [ii for ii in indTest if (X[ii].shape[1]>tTest) and (y[ii][1,0]==1)]  # Fix this to 0
    X,y = feedBatch(inds)
    model = bc.buildModel()
    model.load_weights('./weights/survival.wei')
    ypred = model.predict(X)[:,0]
    r2, mape = evalRM(y[:,0], ypred)
    print r2, mape
    inp = np.eye(bc.inDim)
    yexp = model.predict(inp)
    print yexp
    wei = model.get_weights()
    print np.dot(np.dot(wei[4].T,wei[2]), wei[0].T) 
    # sio.savemat('evalresult2.mat', {'pred':np.exp(-ypred)})