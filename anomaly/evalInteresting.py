from __future__ import division
import numpy as np
import helpers as hlp 
import baseCodeSingle as bc
import cPickle as pk
import pdb
import scipy.io as sio

tTest = 36

with open('NDKdataCFformatted.p', 'rb') as f:
    data = pk.load(f)
    X = data['X']
    y = data['y']
    s = data['s']

def partition(n, testRatio=0.15,validRatio=0.1):
    np.random.seed(0)    # Reproducability
    ind = np.random.permutation(n)
    nTest = int(testRatio*n)
    nValid = int(validRatio*n)
    return ind[nTest+nValid:],ind[0:nTest],ind[nTest:nTest+nValid]      # Train, Test, Validation

def feedBatchExp(ind):
    n, Tmax, dd = len(ind), max([X[ii].shape[1] for ii in ind]), X[0].shape[0]+len(s[0])
    ff = np.zeros((n, tTest, dd))
    ll = np.zeros((n, tTest, 2))
    for (i, jn) in enumerate(ind):
        T = X[jn].shape[1]
        X[jn][~np.isfinite(X[jn])] = 0   ########Careful
        srep = np.tile(np.array(s[jn])[:, np.newaxis], (1, T))
        ff[i, :, :] = np.vstack((X[jn], srep)).T[-tTest:, :]
        ll[i, :, :] = y[jn].T[-tTest:, :]
    return ff, ll

def evalRM(ytrue, ypred):
    pdb.set_trace()
    r2 = 1- ((ytrue-ypred)**2).sum()/((ytrue-ytrue.mean())**2).sum()
    mape = (abs( (ytrue-ypred)/ytrue )).mean()
    return r2, mape

if __name__ == "__main__":
    # Model fitting
    indTrain, indTest, indValid = partition(len(X))
    inds = [ii for ii in indTest if (X[ii].shape[1]>tTest) and (y[ii][1,0]==1)]  # Fix this to 0
    X,y = feedBatchExp(inds)
    model = bc.buildModel()
    model.load_weights('./weights/survival.wei')
    ypred = model.predict(X)[:,:,0]
    r2, mape = evalRM(y[:,:,0]/bc.tscale, np.exp(-ypred))
    print r2, mape
    sio.savemat('evalresult2.mat', {'pred':np.exp(-ypred)})
