import numpy as np 
import helpers as hlp
import matplotlib.pyplot as plt
from lifelines import KaplanMeierFitter, WeibullFitter, BreslowFlemingHarringtonFitter

def drawSurvivalCurves(wellids, algorithm='Kaplan Meier'):
    """ This function plots the survival curve using the wells in wellids.
    First, use your definition of a field and get the well ids as a list.  
    Then, specify the algortihm and run this function.
    """
    if algorithm=='Weibull':
        kmf = WeibullFitter()
    elif algorithm=='Breslow Fleming Harrington':
        kmf = BreslowFlemingHarringtonFitter()
    else:
        kmf = KaplanMeierFitter()

    ltime, obs= hlp.readFromDBsimple(wellids)
    kmf.fit(ltime, event_observed=obs)
    x, y = kmf.survival_function_.index.values, kmf.survival_function_.values
    line = plt.step(x, y)
    plt.setp(line, linewidth=3)
    plt.ylim(0,1.05)
    plt.xlabel('Time (months)')
    plt.ylabel('Survival function')
    plt.title(algorithm + ' Estimator')
    plt.grid(True)
    plt.show()

if __name__ == "__main__":
    # get some well ids.
    wellids = np.genfromtxt('exampleWellids.csv', dtype=long).tolist()
    drawSurvivalCurves(wellids)