# Added by me
def survival_exponential(y_true, y_pred):
    # y_true[:,0]: times, y_true[:,1]: deaths
    return T.mean(y_pred[:,0]*y_true[:,0] - y_true[:,1]*T.log(y_pred[:,0]+epsilon), axis=-1)

def survival_weibul(y_true, y_pred):
    # y_true[:,0]: times, y_true[:,1]: deaths
    d,t,lam,p = y_true[:,1], y_true[:,0], y_pred[:,0], y_pred[:,1]
    return T.mean( T.pow((lam*t+epsilon),p) - d*((p-1)*T.log(lam*t+epsilon)-T.log(p+epsilon)-T.log(lam+epsilon)), axis=-1)

def expert_loss(y_true, y_pred):
    # The loss function for expert aggregation
    return T.sqr(T.dot(y_true[:-1], y_pred[:-1]) - y_true[-1] + T.abs_(y_pred[-1])).mean(axis=-1)

def mse_scaled(y_true, y_pred):
    return 0.01*(T.sqr(y_pred - y_true).mean(axis=-1))

def survival(y_true, y_pred):
    # y_true[:,0]: times, y_true[:,1]: deaths, y_pred[:,0]: log of hazard rate
    return T.mean(T.exp(y_pred[:,0])*y_true[:,0] - y_true[:,1]*y_pred[:,0], axis=-1)