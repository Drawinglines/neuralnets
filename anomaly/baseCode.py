from __future__ import division
from keras.models import Sequential  
from keras.layers.core import TimeDistributedDense, Masking, Dropout  
from keras.layers.recurrent import GRU
from os import fsync
import numpy as np
import helpers as hlp 
import cPickle as pk
import pdb

inDim = 7
nHid = 20
batchSize = 200
doRate = 0.3
nEpoch = 50
tscale = 120

# Load the data globally
with open('NDKdataCFformatted.p', 'rb') as f:
    data = pk.load(f)
    X = data['X']
    y = data['y']
    s = data['s']

# pdb.set_trace()

def getIndexes(ind):
    n = ind.shape[0]
    nBatch = int(np.ceil(n/batchSize))
    index = [ind[range(i*batchSize, min((i+1)*batchSize, n))] for i in range(nBatch)]
    return index

def partition(n, testRatio=0.15,validRatio=0.1):
    np.random.seed(0)    # Reproducability
    ind = np.random.permutation(n)
    nTest = int(testRatio*n)
    nValid = int(validRatio*n)
    return ind[nTest+nValid:],ind[0:nTest],ind[nTest:nTest+nValid]      # Train, Test, Validation

def feedBatch(ind):
    ind = [ii for ii in ind if X[ii].shape[1]>0]
    n, Tmax, dd = len(ind), max([X[ii].shape[1] for ii in ind]), X[0].shape[0]+len(s[0])
    ff = np.zeros((n, Tmax, dd))
    ll = np.zeros((n, Tmax, 2))
    for (i, jn) in enumerate(ind):
        T = X[jn].shape[1]
        X[jn][~np.isfinite(X[jn])] = 0   ########Careful
        srep = np.tile(np.array(s[jn])[:, None], (1, T))
        ff[i, -T:, :] = np.vstack((X[jn], srep)).T
        ll[i, -T:, :] = np.vstack((y[jn][0,:]/tscale, y[jn][1,:])).T
    return ff, ll

def buildModel():
    model = Sequential()  
    model.add(Masking(mask_value=0., input_shape=(None,inDim)))
    model.add(TimeDistributedDense(nHid, activation='tanh'))
    model.add(Dropout(doRate))
    model.add(GRU(nHid, return_sequences=True))
    model.add(Dropout(doRate))
    model.add(TimeDistributedDense(1, activation='linear'))  
    model.compile(loss="survival", optimizer="rmsprop") 
    print "Model compiled."
    return model

def validateBatch(model, index):
    val_score=0
    lastLam = list()
    for ind in index:
        X,y = feedBatch(ind)
        val_score += model.evaluate(X,y, verbose=0)
        lastLam.append(model.predict(X)[:,-1,0])
    print np.mean(np.concatenate(tuple(lastLam)))
    return val_score/len(index)

def train(model, indTrainBatches, indValidBatches):
    logfile = open("log.txt", "wb")
    losses = list()
    bestLoss = np.inf
    for i in range(nEpoch):
        for (b, ind) in enumerate(indTrainBatches):
            X,y = feedBatch(ind)
            loss = model.train_on_batch(X,y)
            print "Epoch #{}\t Batch #{}:\t Training loss:\t {}.".format(i,b,loss)
            if (b+1)%50 == 0:
                losses.append(validateBatch(model, indValidBatches))
                logfile.write("Validation loss at {}th iteration:\t {}\n".format(len(losses), losses[-1]))
                logfile.flush()  # To make sure that it writes
                fsync(logfile)
                if losses[-1] < bestLoss:
                    bestLoss = losses[-1]
                    bestModel = model
                    bestModel.save_weights('./weights/survival.wei', overwrite=True)
    
    logfile.close()
    del losses[0]
    with open('loss.p', 'wb') as f:
        pk.dump(losses, f)
    return bestModel

if __name__ == "__main__":
    # Model fitting
    indTrain, indTest, indValid = partition(len(X))
    indTrainBatches = getIndexes(indTrain)
    indValidBatches = getIndexes(indValid)
    model = buildModel()
    model = train(model, indTrainBatches, indValidBatches)
